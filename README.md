# Spring Employee Manager
## Introduction

It is a simple web application cooperating with a database, created to consolidate my knowledge about Spring MVC and Spring Security, Spring Data JPA has been used in the data persistence layer.

## Features
* Basic operations create, read, update, and delete on the employee repository.
* Associating an employee with the department through a foreign key.
* Resource access control: only a user with administrator rights can delete an employee, a manager can view detailed data, edit it, and add new employees.
* The program allows you to display the department's data, including the department's basic statistics.
* The project also includes tests. The web controller code has been tested with integration tests, the StatisticsCalculator class has unit tests.
* Thymeleaf is used as a template engine.
* A Connection with external api for exchange rates and weather.
* Counter of visits form restart using Spring AOP.
* Authentication using UserDetailedService that fetch users data from a database.
* Pagination of employees content.
* Demo user functionality. It's possible to login as a demo user,
  and to work on a separate database. The demo database is restored to the default state on every demo user login.
## Technologies
* Java 11
* Spring Boot, MVC, Security
* Spring Data JPA
## How to use it?
The easiest way to start the application is to visit it at this link:
http://mvccrudapp2-env.eba-9rcwiq5c.us-east-1.elasticbeanstalk.com/

Use the button for login as a demo user(with a temporary database, only for one log-in)
or choose from the list of defined users:
 <table>
  <tr>
    <th>username</th>
    <th>password</th>
  </tr>
  <tr>
    <td>guest</td>
    <td>guest</td>
  </tr>
  <tr>
    <td>employee</td>
    <td>employee</td>
  </tr>
  <tr>
    <td>manager</td>
    <td>manager</td>
  </tr>
  <tr>
    <td>admin</td>
    <td>admin</td>
  </tr>
</table> 

Another way is to use prepared docker images with docker-compose.   
One of the containers contains a demo database, the other is an image of the actual project.   
The only requirement is to have port 8080 free(or if you want you can redirect it in docker compose).

    ```
    git clone https://gitlab.com/MalecPrzemyslaw/spring_mvc_crud.git #clone project
    cd spring_mvc_crud/
    sudo docker-compose up #start containers
    # after few seconds web app will be ready to use, in a browser under addres: http://localhost:8080
    ```

## Next todo's
* Consider automatically assigning each employee a login account.  
  Login via mail. Account details(passwords, roles) stored in a table in relation to the employee's table
* <s>Add pagination of employee list. Use built-in Spring Data Jpa methods.</s>