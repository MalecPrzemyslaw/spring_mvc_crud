package com.MPrzem.EmployeesMVCCrud.controller;

import com.MPrzem.EmployeesMVCCrud.aop.VisitsCounter;
import com.MPrzem.EmployeesMVCCrud.dataSourceRouting.DemoUserHander;
import com.MPrzem.EmployeesMVCCrud.entity.Department;
import com.MPrzem.EmployeesMVCCrud.externalApi.dto.*;
import com.MPrzem.EmployeesMVCCrud.service.*;
import com.MPrzem.EmployeesMVCCrud.statistics.Statistics;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest
public class DepartmentControllerTests {
    @MockBean
    DepartmentService departmentService;
    @MockBean
    EmployeeService employeeService;
    @MockBean
    ExternalApiService externalApiService;
    @MockBean
    VisitsCounter visitsCounter;
    @MockBean
    DemoUserHander demoUserHander;
    @Autowired
    private MockMvc mockMvc;
    @Before
    public void beforeEachTest(){
        doReturn(new WeatherDto()).when(externalApiService).getWeatherDto();
        doReturn(new CurrencyDto()).when(externalApiService).getCurrencyDto();
    }
    @Test
    @WithMockUser(username="john",roles={"MANAGER"})
    public void show_department_request_should_find_department_and_shows_with_stats() throws Exception {
        Department returnDepartment = new Department();
        Statistics returnStatistics = new Statistics();
        doReturn(returnStatistics).when(departmentService).getStatistics(5);
        doReturn(returnDepartment).when(departmentService).findById(5);

        this.mockMvc.perform(get("/departments/showDepartment")
                    .param("departmentId", "5"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("department",equalTo(returnDepartment)))
                .andExpect(model().attribute("statistics",equalTo(returnStatistics)));
        verify(departmentService,times(1)).findById(5);
        verify(departmentService,times(1)).getStatistics(5);
    }
    @Test
    @WithMockUser(username="john",roles={"GUEST"})
    public void show_department_restricted_for_employees_managers_admins() throws Exception {
        Department returnDepartment = new Department();
        Statistics returnStatistics = new Statistics();
        doReturn(returnStatistics).when(departmentService).getStatistics(5);
        doReturn(returnDepartment).when(departmentService).findById(5);
        this.mockMvc.perform(get("/departments/showDepartment")
                        .param("departmentId", "5"))
                .andExpect(status().isForbidden());
    }
}
