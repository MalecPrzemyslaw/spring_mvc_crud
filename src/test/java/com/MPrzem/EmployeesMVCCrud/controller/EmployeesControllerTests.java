package com.MPrzem.EmployeesMVCCrud.controller;
import com.MPrzem.EmployeesMVCCrud.aop.VisitsCounter;
import com.MPrzem.EmployeesMVCCrud.dataSourceRouting.DemoUserHander;
import com.MPrzem.EmployeesMVCCrud.entity.*;
import com.MPrzem.EmployeesMVCCrud.externalApi.dto.*;
import com.MPrzem.EmployeesMVCCrud.service.*;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebMvcTest
public class EmployeesControllerTests {
	@MockBean
	EmployeeService employeeService;
	@MockBean
	DepartmentService departmentService;
	@MockBean
	ExternalApiService externalApiService;
	@MockBean
	VisitsCounter visitsCounter;
	@MockBean
	DemoUserHander demoUserHander;
	@Autowired
	private MockMvc mockMvc;
	private ArrayList<Employee> employeesList=null;
	private Employee employee=null;
	@Before
	public void setUp() {
		employee = new Employee(1,"name1","name2","e@e.com",
				new Department(1,"Sales","SalesDepartment"),
				1000, java.time.LocalDate.now());
		employeesList = new ArrayList<Employee>() {
			{
				add(employee);
			}
		};
		Mockito.reset(employeeService);
		Mockito.reset(departmentService);
		Mockito.reset(externalApiService);
		Mockito.reset(visitsCounter);

		doReturn(new WeatherDto()).when(externalApiService).getWeatherDto();
		doReturn(new CurrencyDto()).when(externalApiService).getCurrencyDto();
	}
	@Test
	@WithMockUser(username="john",roles={"EMPLOYEE","MANAGER"})
	public void shows_list_of_employees() throws Exception {
		Page<Employee> page = new PageImpl<>(employeesList);
		doReturn(page).when(employeeService).findPage(any());
		this.mockMvc.perform(get("/employees/list"))
				.andExpect(status().isOk())
				.andExpect(model().attributeExists("employees"))
				.andExpect(model().attribute("employees",page));
		verify(employeeService,times(1)).findPage(any());
		verify(employeeService,times(0)).findAll();
	}
	@Test
	@WithMockUser(username="john",roles={"EMPLOYEE","MANAGER"})
	public void shows_requested_page_of_employees() throws Exception {
		Page<Employee> page = new PageImpl<>(employeesList);
		Mockito.reset(employeeService);
		doReturn(page).when(employeeService).findPage(any());
		this.mockMvc.perform(get("/employees/list?page=2"))
				.andExpect(status().isOk())
				.andExpect(model().attributeExists("employees"))
				.andExpect(model().attribute("employees",page));
		verify(employeeService,times(1)).findPage(argThat((pageable -> pageable.getPageNumber()==2)));
		verify(employeeService,times(0)).findAll();
	}
	@Test
	@WithMockUser(username="john",roles={"EMPLOYEE","MANAGER"})
	public void update_request_shows_update_form() throws Exception {
		doReturn(employee).when(employeeService).findById(1);
		this.mockMvc.perform(get("/employees/update")
				.param("employeeId", "1"))
				.andExpect(status().isOk())
				.andExpect(view().name("employees/employee-form"))
				.andExpect(model().attributeExists("employee"))
				.andExpect(model().attribute("employee",equalTo(employee)));
	}
	@Test
	@WithMockUser(username="john",roles={"EMPLOYEE","MANAGER"})
	public void update_request_throws_exeption_when_cant_find_user() throws Exception {
		doThrow(new RuntimeException("can't find user")).when(employeeService).findById(anyInt());
		Assertions.assertThatThrownBy(()->this.mockMvc.perform(get("/employees/update")
			.param("employeeId", "2"))
			.andExpect(status().isBadRequest())).hasCause(new RuntimeException("can't find user"));
	}
	@Test
	@WithMockUser(username="mary",roles={"MANAGER"})
	public void save_request_with_correct_data_calls_employeesService_save() throws Exception {
		doNothing().when(employeeService).save(employee);
		this.mockMvc.perform(post("/employees/save").with(csrf())
				.flashAttr("employee",employee))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/employees/list"));
		verify(employeeService,times(1)).save(employee);
	}
	@Test
	@WithMockUser(username="mary",roles={"MANAGER"})
	public void save_request_with_wrong_data_detect_errors() throws Exception {
		this.mockMvc.perform(post("/employees/save").with(csrf())
				.flashAttr("employee", new Employee(1,"","","e@ecom")))
				.andExpect(model().errorCount(5))
				.andExpect(view().name("employees/employee-form"));//return again to employee-form
		verify(employeeService,times(0)).save(any());
	}
	@Test
	@WithMockUser(username="mary",roles={"ADMIN"})
	public void delete_request_should_delete_user_if_correct_id() throws Exception {
		this.mockMvc.perform(get("/employees/delete")
						.param("employeeId", "2"))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/employees/list"));
		verify(employeeService,times(1)).deleteById(2);
	}
	@Test
	@WithMockUser(username="mary",roles={"EMPLOYEE"})
	public void delete_request_should_redirect_to_acces_deined_if_no_ADMIN_role() throws Exception {
		this.mockMvc.perform(get("/employees/delete")
						.param("employeeId", "2"))
				.andExpect(status().isForbidden())
				.andExpect(forwardedUrl("/access-denied"));
		verify(employeeService,times(0)).deleteById(2);
	}
	@Test
	@WithMockUser(username="mary",roles={"EMPLOYEE"})
	public void search_request_should_call_employees_find() throws Exception {
		Page<Employee> passedEmployees = new PageImpl<>(employeesList);
		doReturn(passedEmployees).when(employeeService).searchBy(eq("ada"),any());
		this.mockMvc.perform(get("/employees/search")
						.param("employeeName", "ada"))
				.andExpect(status().isOk())
				.andExpect(model().attributeExists("employees"))
				.andExpect(model().attribute("employees",passedEmployees));
		verify(employeeService,times(1)).searchBy(eq("ada"),any());
	}
}

