package com.MPrzem.EmployeesMVCCrud.dataSourceRouting;

import com.MPrzem.EmployeesMVCCrud.aop.VisitsCounter;
import com.MPrzem.EmployeesMVCCrud.service.DepartmentService;
import com.MPrzem.EmployeesMVCCrud.service.EmployeeService;
import com.MPrzem.EmployeesMVCCrud.service.ExternalApiService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class DemoUserHandlerTest {
    @MockBean
    EmployeeService employeeService;
    @MockBean
    DepartmentService departmentService;
    @MockBean
    ExternalApiService externalApiService;
    @MockBean
    VisitsCounter visitsCounter;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    Authentication authentication;
    @Mock
    UrlPathHelper urlPathHelper;
    DemoUserHander demoUserHander;
    private static final String redirPath = "/dummyPath";
    @Before
    public void setUp(){
        demoUserHander = new DemoUserHander(employeeService,departmentService,urlPathHelper);
        when(urlPathHelper.getContextPath(any())).thenReturn(redirPath);

    }
    @Test
    @WithMockUser(username="john",roles={"EMPLOYEE","MANAGER"})
    public void shouldRedirect(){
        try {
            demoUserHander.onAuthenticationSuccess(request,response,authentication);
        }catch (Exception e ){
            Assert.fail();
        }
        try {
            verify(response,times(1)).sendRedirect(redirPath);
        } catch (IOException e) {
            Assert.fail();
        }
    }
    @Test
    @WithMockUser(username=DbRouter.DEMOUSERNAME,roles={"MANAGER"})
    public void if_demo_user_should_refresh_demo_db_data(){
        try {
            demoUserHander.onAuthenticationSuccess(request,response,authentication);
        }catch (Exception e ){
            Assert.fail();
        }
        verify(employeeService,times(1)).deleteAll();
        verify(employeeService,times(1)).saveAll(any());
        verify(departmentService,times(1)).saveAll(any());
        verify(departmentService,times(1)).deleteAll();
    }
}
