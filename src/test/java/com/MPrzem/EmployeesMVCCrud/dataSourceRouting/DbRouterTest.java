package com.MPrzem.EmployeesMVCCrud.dataSourceRouting;

import com.MPrzem.EmployeesMVCCrud.aop.VisitsCounter;
import com.MPrzem.EmployeesMVCCrud.service.DepartmentService;
import com.MPrzem.EmployeesMVCCrud.service.EmployeeService;
import com.MPrzem.EmployeesMVCCrud.service.ExternalApiService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class DbRouterTest {
    @MockBean
    EmployeeService employeeService;
    @MockBean
    DepartmentService departmentService;
    @MockBean
    ExternalApiService externalApiService;
    @MockBean
    VisitsCounter visitsCounter;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    Authentication authentication;

    @Test
    @WithMockUser(username=DbRouter.DEMOUSERNAME,roles={"MANAGER"})
    public void DbRouter_should_choose_accurate_datasoruce_demo_for_demo_user(){
        DbRouter dbRouter = new DbRouter();
        var key = (DbUsersType)  dbRouter.determineCurrentLookupKey();
        Assert.assertEquals(key,DbUsersType.DEMO_CLIENT);
    }
    @Test
    @WithMockUser(username="admin",roles={"MANAGER"})
    public void DbRouter_should_choose_accurate_datasoruce_for_not_demo_user(){
        DbRouter dbRouter = new DbRouter();
        var key = (DbUsersType)  dbRouter.determineCurrentLookupKey();
        Assert.assertEquals(key,DbUsersType.NORMAL_CLIENT);
    }
}
