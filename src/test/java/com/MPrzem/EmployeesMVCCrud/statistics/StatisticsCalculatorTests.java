package com.MPrzem.EmployeesMVCCrud.statistics;

import com.MPrzem.EmployeesMVCCrud.entity.*;
import org.junit.*;

import java.time.LocalDate;
import java.util.*;

public class StatisticsCalculatorTests {
    StatisticsCalculator calculator;
    Department department;
    @Before
    public void InitStandardInputData(){
        calculator = new StatisticsCalculator();
        Employee employee1 = new Employee();
        employee1.setSalary(1000);
        employee1.setEmployment_date(LocalDate.of(2000,1,1));
        Employee employee2 = new Employee();
        employee2.setSalary(3000);
        employee2.setEmployment_date(LocalDate.of(2009,10,1));
        department = new Department(1,"HR","Some department description");
        List<Employee> employeeList= new ArrayList<Employee>(){
            {

              add(employee1);
              add(employee2);
            }
        };
        department.setEmployees(employeeList);
    }

    @Test
    public void should_calculate_correct_seniority_average(){
        Statistics stats = calculator.calculateSatatisticsDepartment(department,LocalDate.of(2020,1,1));
        Assert.assertEquals(15,stats.getAverageSeniorityYears());
        Assert.assertEquals(1,stats.getAverageSeniorityMoths());
    }
    @Test
    public void should_calculate_correct_salary_average(){
        Statistics stats = calculator.calculateSatatisticsDepartment(department,LocalDate.of(2020,1,1));
        Assert.assertEquals(2000,stats.getAverageSalary(),0.01);
    }
    @Test
    public void should_calculate_correct_employment(){
        Statistics stats = calculator.calculateSatatisticsDepartment(department,LocalDate.of(2020,1,1));
        Assert.assertEquals(2,stats.getEmployment());
    }
    @Test
    public void should_calculate_salary_min_max(){
        Statistics stats = calculator.calculateSatatisticsDepartment(department,LocalDate.of(2020,1,1));
        Assert.assertEquals(1000,stats.getLowestSalary());
        Assert.assertEquals( 3000,stats.getHighestSalary());
    }
    @Test
    public void should_return_default_values_if_empty_department(){
        Statistics stats = calculator.calculateSatatisticsDepartment(new Department(),LocalDate.of(2020,1,1));
        Assert.assertEquals(Integer.MAX_VALUE,stats.getLowestSalary());
        Assert.assertEquals( 0,stats.getHighestSalary());
        Assert.assertEquals(0,stats.getEmployment());
        Assert.assertEquals(0,stats.getAverageSalary(),0.01);
        Assert.assertEquals(0,stats.getAverageSeniorityYears());
        Assert.assertEquals(0,stats.getAverageSeniorityMoths());
    }
}
