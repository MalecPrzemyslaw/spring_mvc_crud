package com.MPrzem.EmployeesMVCCrud.externalApi;

import com.MPrzem.EmployeesMVCCrud.externalApi.dto.WeatherDto;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class WeatherClientTests {
    @Mock
    ApiClient apiClient;
    WeatherClient weatherClient;

    WeatherClient.WeatherPOJO weatherPOJO;
    @Before
    public void init() {
        weatherPOJO = new WeatherClient.WeatherPOJO();
        WeatherClient.Wind wind = new WeatherClient.Wind();
        WeatherClient.Main main = new WeatherClient.Main();
        WeatherClient.WeatherDescription[] weather =
                new WeatherClient.WeatherDescription[]{new WeatherClient.WeatherDescription("test description")};
        weatherPOJO.setWeather(weather);
        weatherPOJO.setMain(main);
        weatherPOJO.setWind(wind);
        weatherClient = new WeatherClient(apiClient,"apiKey");
    }

    @Test
    public void currency_client_should_map_response_body_to_dto(){
        weatherPOJO.getMain().setTemp(20);
        weatherPOJO.getWind().setSpeed(10);
        Mockito.when(apiClient.callGetMethod(any(),any(),any())).thenReturn(weatherPOJO);
        WeatherDto weatherDto= weatherClient.getCurrentWeather("warszawa");
        Assert.assertEquals(weatherDto.getDescription(),weatherPOJO.getWeather()[0].getDescription());
        Assert.assertEquals(weatherDto.getTemp(),weatherPOJO.getMain().getTemp(),0.01);
        Assert.assertEquals(weatherDto.getSpeed(),weatherDto.getSpeed(),0.01);
    }
    @Test
    public void currency_client_should_request_data_for_specified_city_and_key() {
        Mockito.when(apiClient.callGetMethod(any(),any(),any())).thenReturn(weatherPOJO);
        weatherClient.getCurrentWeather("warszawa");
        Mockito.verify(apiClient, Mockito.times(1)).
                callGetMethod(any(), any(), eq("warszawa"),eq("apiKey"));
    }
    @Test
    public void currency_client_should_request_data_using_key() {
        Mockito.when(apiClient.callGetMethod(any(),any(),any())).thenReturn(weatherPOJO);
        weatherClient.getCurrentWeather("warszawa");
        Mockito.verify(apiClient, Mockito.times(1)).
                callGetMethod(any(), any(), any(),eq("apiKey"));
    }
}
