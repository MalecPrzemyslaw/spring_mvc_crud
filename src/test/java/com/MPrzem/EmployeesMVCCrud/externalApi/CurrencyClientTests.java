package com.MPrzem.EmployeesMVCCrud.externalApi;

import com.MPrzem.EmployeesMVCCrud.externalApi.dto.CurrencyDto;
import com.MPrzem.EmployeesMVCCrud.externalApi.CurrencyClient;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.AdditionalMatchers.and;
import static org.mockito.ArgumentMatchers.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class CurrencyClientTests {
    @Mock
    ApiClient apiClient;
    CurrencyClient currencyClient;
    @Before
    public void init() {
        currencyClient = new CurrencyClient(apiClient);
    }

    @Test
    public void currency_client_should_map_response_body_to_dto(){
        CurrencyClient.CurrencyPojo currencyPojo = new CurrencyClient.CurrencyPojo();
        CurrencyClient.Rates rates = new CurrencyClient.Rates();
        rates.setCHF(5);
        rates.setGBP(4);
        rates.setEUR(55);
        rates.setPLN(1);
        currencyPojo.setConversion_rates(rates);
        Mockito.when(apiClient.callGetMethod(any(),any(),any())).thenReturn(currencyPojo);
        CurrencyDto currencyDto= currencyClient.getCurrentCurrency();
        Assert.assertEquals(currencyDto.getChf(),rates.getCHF(),0.01);
        Assert.assertEquals(currencyDto.getPln(),rates.getPLN(),0.01);
        Assert.assertEquals(currencyDto.getGbp(),rates.getGBP(),0.01);
        Assert.assertEquals(currencyDto.getEur(),rates.getEUR(),0.01);
    }
    @Test
    public void currency_client_should_call_latest_endpoint_for_USD() {
        CurrencyClient.CurrencyPojo currencyPojo = new CurrencyClient.CurrencyPojo();
        CurrencyClient.Rates rates = new CurrencyClient.Rates();
        rates.setCHF(5);
        rates.setGBP(4);
        rates.setEUR(55);
        rates.setPLN(1);
        currencyPojo.setConversion_rates(rates);
        Mockito.when(apiClient.callGetMethod(any(), any(), any())).thenReturn(currencyPojo);
        CurrencyDto currencyDto = currencyClient.getCurrentCurrency();
        Mockito.verify(apiClient, Mockito.times(1)).
                callGetMethod(and(contains("latest"),contains("USD")), any(),any());
    }
}
