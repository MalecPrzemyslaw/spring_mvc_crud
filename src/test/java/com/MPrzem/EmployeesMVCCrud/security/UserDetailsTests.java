package com.MPrzem.EmployeesMVCCrud.security;

import com.MPrzem.EmployeesMVCCrud.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;

public class UserDetailsTests {
    @Test
    public void user_details_should_map_userDTO_roles_to_list_of_authorities(){
        User user = new User();
        user.setRoles("ADMIN,EMPLOYEE,SOMEOTHER");
        user.setName("");
        user.setPassword("");

        ArrayList<GrantedAuthority> expectation = new ArrayList<>() {
            {
                add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                add(new SimpleGrantedAuthority("ROLE_EMPLOYEE"));
                add(new SimpleGrantedAuthority("ROLE_SOMEOTHER"));
            }};

        UserDetails userDetails = new UserDetails(user);
        var auths = userDetails.getAuthorities();

        Assert.assertEquals(auths.size(),3);
        Assert.assertTrue(userDetails.getAuthorities().containsAll(expectation));
    }
    @Test
    public void user_details_should_return_isEnabled_corresponding_to_userDTO_isActive() {
        User user = new User();
        user.setRoles("");
        user.setName("");
        user.setPassword("");
        user.setActive(true);
        UserDetails userDetails = new UserDetails(user);
        Assert.assertTrue(userDetails.isEnabled());
        user.setActive(false);
        Assert.assertFalse(userDetails.isEnabled());
    }
    @Test
    public void user_details_should_throw_illegal_arg_if_data_input_is_incomplete() {
        User user = new User();
        user.setName("");
        user.setPassword("");
        user.setActive(true);
        Assert.assertThrows(IllegalArgumentException.class,()-> new UserDetails(user));
    }
}
