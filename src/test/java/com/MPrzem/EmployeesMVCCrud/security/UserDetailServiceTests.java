package com.MPrzem.EmployeesMVCCrud.security;

import com.MPrzem.EmployeesMVCCrud.dao.UserRepository;
import com.MPrzem.EmployeesMVCCrud.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class UserDetailServiceTests {

    @Mock
    UserRepository userRepository;

    UserDetailsDbService userDetailsDbService;
    User user;
    @Before
    public void init(){
        userDetailsDbService = new UserDetailsDbService(userRepository);
        user = new User();
        user.setRoles("ROLE_ADMIN");
        user.setName("Name");
        user.setPassword("123");
        user.setActive(true);
        when(userRepository.findUserByName(any())).thenReturn(Optional.of(user));

    }
    @Test
    public void should_reguest_repository_for_particular_user(){
        String request= "requested name";
        userDetailsDbService.loadUserByUsername(request);
        verify(userRepository,times(1)).findUserByName(any());

    }
    @Test
    public void should_not_catch_UsernameNotFoundException(){
        String request= "requested name";
        when(userRepository.findUserByName(any())).thenReturn(Optional.empty());
        Assert.assertThrows(UsernameNotFoundException.class,()->userDetailsDbService.loadUserByUsername(request));
    }
}
