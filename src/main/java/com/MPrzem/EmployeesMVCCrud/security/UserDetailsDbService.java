package com.MPrzem.EmployeesMVCCrud.security;

import com.MPrzem.EmployeesMVCCrud.dao.UserRepository;
import com.MPrzem.EmployeesMVCCrud.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsDbService implements UserDetailsService {
    UserRepository userRepository;

    public UserDetailsDbService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       Optional<User> userOptional= userRepository.findUserByName(username);
       User user =userOptional.orElseThrow(()->new UsernameNotFoundException(username+" not found"));
       return new com.MPrzem.EmployeesMVCCrud.security.UserDetails(user);
    }
}
