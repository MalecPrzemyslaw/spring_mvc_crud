package com.MPrzem.EmployeesMVCCrud.security;

import com.MPrzem.EmployeesMVCCrud.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {
    final User user;
    final Collection<? extends GrantedAuthority> authorities;

    public UserDetails(User user){
        this.user = user;
        if(user.getRoles()==null||user.getName()==null||user.getPassword()==null){
            throw new IllegalArgumentException("User data incomplete.");
        }
        authorities = Arrays.stream(user.getRoles().split(","))
                .map((a)->new SimpleGrantedAuthority("ROLE_"+a))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isActive();
    }
}
