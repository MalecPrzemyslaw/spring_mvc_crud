package com.MPrzem.EmployeesMVCCrud.controller;

import com.MPrzem.EmployeesMVCCrud.service.*;
import com.MPrzem.EmployeesMVCCrud.statistics.Statistics;
import com.MPrzem.EmployeesMVCCrud.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/departments")
public class DepartmentController {
    private DepartmentService departmentService;
    private ExternalApiService externalApiService;
    @Autowired
    public DepartmentController(DepartmentService departmentService,ExternalApiService externalApiService) {
        this.departmentService = departmentService;
        this.externalApiService = externalApiService;
    }

    @GetMapping("/showDepartment")
    public String showDepartmentForm(@RequestParam("departmentId") int theId,
                                     Model theModel) {

        Department theDepartment = departmentService.findById(theId);
        Statistics theStatistics = departmentService.getStatistics(theId);
        theModel.addAttribute("department",theDepartment);
        theModel.addAttribute("statistics",theStatistics);
        theModel.addAttribute("weather",externalApiService.getWeatherDto());
        theModel.addAttribute("currency",externalApiService.getCurrencyDto());
        return "department/show-department";
    }

}
