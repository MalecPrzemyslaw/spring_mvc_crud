package com.MPrzem.EmployeesMVCCrud.controller;

import com.MPrzem.EmployeesMVCCrud.aop.VisitsCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/adminStats")
public class VisitsCounterRestController {
    @Autowired
    VisitsCounter visitsCounter;
    @GetMapping("/visitsCounter")
    int visits() {
        return visitsCounter.getCount();
    }
}
