package com.MPrzem.EmployeesMVCCrud.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.MPrzem.EmployeesMVCCrud.aop.EntryPointAnnotation;
import com.MPrzem.EmployeesMVCCrud.entity.*;
import com.MPrzem.EmployeesMVCCrud.service.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;
import com.MPrzem.EmployeesMVCCrud.service.EmployeeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import javax.validation.Valid;

@Controller
@RequestMapping("/employees")
public class EmployeeController {

	private EmployeeService employeeService;
	private DepartmentService departmentService;
	private ExternalApiService externalApiService;

	public EmployeeController(EmployeeService employeeService,DepartmentService departmentService,
							  ExternalApiService externalApiService) {
		this.employeeService = employeeService;
		this.departmentService = departmentService;
		this.externalApiService = externalApiService;
	}
	private Model prepareModelForAdd(Model theModel){
		if(!theModel.containsAttribute("departments")){
			List<Department> departments = departmentService.findAll();
			theModel.addAttribute("departments",departments);
		}
		if(!theModel.containsAttribute("employee")){
			Employee theEmployee = new Employee();
			theModel.addAttribute("employee", theEmployee);
		}
		return theModel;
	}
	@GetMapping("/list")
	public String listBooks(
			@RequestParam("page") Optional<Integer> pageNb,
			@RequestParam("size") Optional<Integer> pageSize,
			Model model){

		Page<Employee> employeesPage = employeeService.findPage(PageRequest. of(pageNb.orElse(0), pageSize.orElse(5)));

		model.addAttribute("pageNumbers", employeeService.getListOfNumberOfPages(employeesPage));
		model.addAttribute("employees", employeesPage);
		model.addAttribute("weather",externalApiService.getWeatherDto());
		model.addAttribute("currency",externalApiService.getCurrencyDto());

		return "employees/list-employees";
	}
	@GetMapping("/entry")
	@EntryPointAnnotation
	public String entry(Model theModel) {
		return "redirect:/employees/list";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		theModel = prepareModelForAdd(theModel);
		return "employees/employee-form";
	}

	@GetMapping("/update")
	public String showFormForUpdate(@RequestParam("employeeId") int theId,
									Model theModel) {
		
		Employee theEmployee = employeeService.findById(theId);
		theModel.addAttribute("employee", theEmployee);
		theModel = prepareModelForAdd(theModel);
		return "employees/employee-form";
	}
	
	
	@PostMapping("/save")
	public String saveEmployee(@Valid @ModelAttribute("employee") Employee theEmployee,
							   BindingResult bindingResult,
							   Model theModel) {

		if(bindingResult.hasErrors()){
			theModel=prepareModelForAdd(theModel);
			return "employees/employee-form";
		}
		if(theEmployee.getDepartment().getId()==0){
			prepareModelForAdd(theModel);
			bindingResult.addError(new ObjectError("department","Choose department"));
			return "employees/employee-form";
		}
		theEmployee.setEmployment_date(LocalDate.now());
		employeeService.save(theEmployee);
		return "redirect:/employees/list";
	}
	
	
	@GetMapping("/delete")
	public String delete(@RequestParam("employeeId") int theId) {
		
		employeeService.deleteById(theId);
		return "redirect:/employees/list";
	}
	
	@GetMapping("/search")
	public String delete(@RequestParam("employeeName") String theName,
						 @RequestParam("page") Optional<Integer> pageNb,
						 @RequestParam("size") Optional<Integer> pageSize,
						 Model model) {
		Page<Employee> employeesPage = employeeService.searchBy(theName,
				PageRequest.of(pageNb.orElse(0), pageSize.orElse(5)));
		model.addAttribute("pageNumbers", employeeService.getListOfNumberOfPages(employeesPage));
		model.addAttribute("employees", employeesPage);
		model.addAttribute("weather",externalApiService.getWeatherDto());
		model.addAttribute("currency",externalApiService.getCurrencyDto());
		return "employees/list-employees";
	}
}
