package com.MPrzem.EmployeesMVCCrud.dao;

import com.MPrzem.EmployeesMVCCrud.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department,Integer> {
}
