package com.MPrzem.EmployeesMVCCrud.dao;

import java.util.List;
import java.util.Optional;

import com.MPrzem.EmployeesMVCCrud.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	public Page<Employee> findAllByOrderByLastNameAsc(Pageable pageable);
	public List<Employee> findAllByOrderByLastNameAsc();
	public Page<Employee> findByFirstNameContainsOrLastNameContainsAllIgnoreCase(String name, String lName,Pageable pageable);
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	void deleteById(Integer id);
	@Lock(LockModeType.PESSIMISTIC_READ)
	Optional<Employee> findById(Integer id);
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	Employee save(Employee entity);
}
