package com.MPrzem.EmployeesMVCCrud.statistics;

import com.MPrzem.EmployeesMVCCrud.entity.Employee;
import com.MPrzem.EmployeesMVCCrud.entity.Department;

import java.time.LocalDate;
import java.time.Period;

public class StatisticsCalculator {
    Period totalSeniority;

    public StatisticsCalculator(Period totalSeniority, int totalSalary, int minSalary, int maxSalary) {
        this.totalSeniority = totalSeniority;
        this.totalSalary = totalSalary;
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
    }

    public StatisticsCalculator() {
        reset();
    }

    int totalSalary;
    int minSalary;
    int maxSalary;
    private void reset(){
        totalSeniority=Period.ZERO;
        totalSalary=0;
        minSalary=Integer.MAX_VALUE;
        maxSalary=0;
    }
    private void collectData(Department department,LocalDate todayData){
        for (Employee employee:department.getEmployees()) {
            Period employeeSenioriy = Period.between(employee.getEmployment_date(),todayData);
            totalSeniority=totalSeniority.plus(employeeSenioriy);
            totalSalary+=employee.getSalary();
            minSalary=Math.min(employee.getSalary(),minSalary);
            maxSalary=Math.max(employee.getSalary(),maxSalary);
        }
    }
    public Statistics calculateSatatisticsDepartment(Department department, LocalDate todayDate){
        reset();
        collectData(department,todayDate);
        int employement = department.getEmployees().size();
        int averageMonths = employement==0?0:(int)(totalSeniority.toTotalMonths()/employement);
        int averageSalary = employement==0?0:totalSalary/employement;
        return new Statistics(averageSalary,minSalary,maxSalary,averageMonths%12,averageMonths/12,employement);
    }

}
