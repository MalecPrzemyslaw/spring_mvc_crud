package com.MPrzem.EmployeesMVCCrud.statistics;

public class Statistics {
    private double averageSalary;
    private int lowestSalary;
    private int highestSalary;
    private int averageSeniorityMoths;
    private int averageSeniorityYears;
    private int employment;

    public int getEmployment() {
        return employment;
    }

    public void setEmployment(int employment) {
        this.employment = employment;
    }

    public Statistics(double averageSalary, int lowestSalary, int highestSalary, int averageSeniorityMoths,
                      int averageSeniorityYears, int employment) {
        this.averageSalary = averageSalary;
        this.lowestSalary = lowestSalary;
        this.highestSalary = highestSalary;
        this.averageSeniorityMoths = averageSeniorityMoths;
        this.averageSeniorityYears = averageSeniorityYears;
        this.employment = employment;
    }

    public int getAverageSeniorityMoths() {
        return averageSeniorityMoths;
    }

    public void setAverageSeniorityMoths(int averageSeniorityMoths) {
        this.averageSeniorityMoths = averageSeniorityMoths;
    }

    public int getAverageSeniorityYears() {
        return averageSeniorityYears;
    }

    public void setAverageSeniorityYears(int averageSeniorityYears) {
        this.averageSeniorityYears = averageSeniorityYears;
    }

    public int getLowestSalary() {
        return lowestSalary;
    }

    public void setLowestSalary(int lowestSalary) {
        this.lowestSalary = lowestSalary;
    }

    public int getHighestSalary() {
        return highestSalary;
    }

    public void setHighestSalary(int highestSalary) {
        this.highestSalary = highestSalary;
    }

    public double getAverageSalary() {
        return averageSalary;
    }

    public void setAverageSalary(double averageSalary) {
        this.averageSalary = averageSalary;
    }

    public Statistics() {
    }

}
