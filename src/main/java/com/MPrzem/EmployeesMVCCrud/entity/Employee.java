package com.MPrzem.EmployeesMVCCrud.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@Table(name="employee")
public class Employee {

	// define fields
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="first_name")
	@NotBlank(message = "First Name is mandatory")
	private String firstName;
	
	@Column(name="last_name")
	@NotBlank(message = "Last Name is mandatory")
	private String lastName;
	
	@Column(name="email")
	@Email(message = "Not valid format of the email",regexp = ".*@.*?\\..*")
	private String email;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "department_id")
	@NotNull(message = "Department cannot be null")
	private Department department;
	@Column(name = "salary")
	@NotNull(message = "Salary is mandatory.")
	@Min(value = 0,message = "Salary should be a positive number")
	private int salary;

	public Employee(String firstName, String lastName, String email, Department department, int salary, LocalDate employment_date) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.department = department;
		this.salary = salary;
		this.employment_date = employment_date;
	}

	@Column(name="employment_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message = "employment date is required")
	private LocalDate employment_date;
		
	// define constructors

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public LocalDate getEmployment_date() {
		return employment_date;
	}

	public void setEmployment_date(LocalDate employment_date) {
		this.employment_date = employment_date;
	}

	public Employee(int id, String firstName, String lastName, String email, Department department, int salary, LocalDate employment_date) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.department = department;
		this.salary = salary;
		this.employment_date = employment_date;
	}

	public Employee() {
		
	}
	
	public Employee(int id, String firstName, String lastName, String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}


	public Employee(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	// define getter/setter
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	// define tostring

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}
		
}











