package com.MPrzem.EmployeesMVCCrud.service;

import com.MPrzem.EmployeesMVCCrud.externalApi.dto.*;

public interface ExternalApiService {
    public WeatherDto getWeatherDto();

    public void setWeatherDto(WeatherDto weatherDto);

    public CurrencyDto getCurrencyDto();

    public void setCurrencyDto(CurrencyDto currencyDto);
}
