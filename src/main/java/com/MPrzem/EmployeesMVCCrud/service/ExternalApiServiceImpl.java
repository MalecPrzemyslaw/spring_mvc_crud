package com.MPrzem.EmployeesMVCCrud.service;

import com.MPrzem.EmployeesMVCCrud.externalApi.dto.*;
import com.MPrzem.EmployeesMVCCrud.externalApi.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.locks.*;

@Service
public class ExternalApiServiceImpl implements ExternalApiService {
    @Autowired
    WeatherClient weatherClient;
    @Autowired
    CurrencyClient currencyClient;
    Lock weatherLock = new ReentrantLock();
    Lock currencyLock = new ReentrantLock();
    private WeatherDto weatherDto = new WeatherDto();
    private CurrencyDto currencyDto = new CurrencyDto();
    @Scheduled(fixedDelay = 3600000)
    public void refreshCurrency(){
        var currencyData = currencyClient.getCurrentCurrency();
        setCurrencyDto(currencyData);
    }
    @Scheduled(fixedDelay = 3600000)
    public void refreshWeather(){
        var weatherData = weatherClient.getCurrentWeather("warszawa");
        setWeatherDto(weatherData);
    }
    @PostConstruct
    public void loadData(){
        refreshCurrency();
        refreshWeather();
    }
    @Override
    public WeatherDto getWeatherDto() {
        try{
            weatherLock.lock();
            WeatherDto copy = new WeatherDto(weatherDto);
            return copy;
        }finally {
            weatherLock.unlock();
        }
    }
    @Override
    public void setWeatherDto(WeatherDto weatherDto) {
        try{
            weatherLock.lock();
            this.weatherDto = weatherDto;
        }finally {
            weatherLock.unlock();
        }
    }
    @Override
    public CurrencyDto getCurrencyDto() {
        try{
            currencyLock.lock();
            CurrencyDto copy = new CurrencyDto(currencyDto);
            return copy;
        }finally {
            currencyLock.unlock();
        }
    }

    @Override
    public void setCurrencyDto(CurrencyDto currencyDto) {
        try{
            currencyLock.lock();
            this.currencyDto = currencyDto;
        }finally {
            currencyLock.unlock();
        }
    }
}
