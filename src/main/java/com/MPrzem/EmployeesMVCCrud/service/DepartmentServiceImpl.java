package com.MPrzem.EmployeesMVCCrud.service;

import com.MPrzem.EmployeesMVCCrud.dao.DepartmentRepository;
import com.MPrzem.EmployeesMVCCrud.statistics.*;
import com.MPrzem.EmployeesMVCCrud.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.*;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    private DepartmentRepository departmentRepository;
    private StatisticsCalculator statisticsCalculator;
    @Autowired
    public DepartmentServiceImpl(DepartmentRepository theEmployeeRepository,StatisticsCalculator theStatisticsCalculator) {
        departmentRepository = theEmployeeRepository;
        statisticsCalculator = theStatisticsCalculator;
    }

    @Override
    public List<Department> findAll() {
        return departmentRepository.findAll();
    }
    @Override
    public Department findById(int theId){
        Optional<Department> result = departmentRepository.findById(theId);
        if (result.isPresent()) {
            return result.get();
        }
        else {
            throw new RuntimeException("Did not find employee id - " + theId);
        }
    }

    @Override
    public Statistics getStatistics(int theId) {
        Optional<Department> result = departmentRepository.findById(theId);

        Employee theEmployee = null;

        if (result.isPresent()) {
            Statistics stats= statisticsCalculator.calculateSatatisticsDepartment(result.get(), LocalDate.now());
            return stats;
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + theId);
        }
    }

    @Override
    public void deleteAll() {
        departmentRepository.deleteAll();
    }
    @Override
    public void saveAll(List<Department> departments) {
        departmentRepository.saveAll(departments);
    }
    @Override
    public void save(Department department) {
        departmentRepository.save(department);
    }

}
