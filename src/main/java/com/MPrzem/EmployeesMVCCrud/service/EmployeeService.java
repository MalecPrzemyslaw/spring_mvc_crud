package com.MPrzem.EmployeesMVCCrud.service;

import java.util.List;

import com.MPrzem.EmployeesMVCCrud.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {

	public List<Employee> findAll();
	public void deleteAll();
	public Page<Employee> findPage(Pageable pageable);
	public List<Integer> getListOfNumberOfPages(Page<Employee> employeePage);
	public Employee findById(int theId);
	
	public void save(Employee theEmployee);
	public void saveAll(List<Employee> employees);
	
	public void deleteById(int theId);

	public Page<Employee> searchBy(String theName,Pageable pageable);
	
}
