package com.MPrzem.EmployeesMVCCrud.service;

import com.MPrzem.EmployeesMVCCrud.entity.Department;
import com.MPrzem.EmployeesMVCCrud.statistics.Statistics;

import java.util.List;

public interface DepartmentService {
    public List<Department> findAll();
    public Department findById(int theId);

    public Statistics getStatistics(int theId);
    public void deleteAll();

    void saveAll(List<Department> departments);

    public void save(Department department);
}
