package com.MPrzem.EmployeesMVCCrud.service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.MPrzem.EmployeesMVCCrud.entity.Department;
import com.MPrzem.EmployeesMVCCrud.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.MPrzem.EmployeesMVCCrud.dao.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository employeeRepository;
	
	@Autowired
	public EmployeeServiceImpl(EmployeeRepository theEmployeeRepository) {
		employeeRepository = theEmployeeRepository;
	}
	
	@Override
	public List<Employee> findAll() {
		return employeeRepository.findAllByOrderByLastNameAsc();
	}

	@Override
	public void deleteAll() {
		employeeRepository.deleteAll();
	}
	@Override
	public Page<Employee> findPage(Pageable pageable){
		return employeeRepository.findAllByOrderByLastNameAsc(pageable);
	}
	@Override
	public List<Integer> getListOfNumberOfPages(Page<Employee> employeePage){
		int totalPages = employeePage.getTotalPages();
		if (totalPages > 0) {
			return IntStream.rangeClosed(0, totalPages-1)
					.boxed()
					.collect(Collectors.toList());
		}
		else return new ArrayList<Integer>();
	}


	@Override
	public Employee findById(int theId) {
		Optional<Employee> result = employeeRepository.findById(theId);
		
		Employee theEmployee = null;
		
		if (result.isPresent()) {
			theEmployee = result.get();
		}
		else {
			// we didn't find the employee
			throw new RuntimeException("Did not find employee id - " + theId);
		}
		
		return theEmployee;
	}

	@Override
	public void save(Employee theEmployee) {
		employeeRepository.save(theEmployee);
	}

	@Override
	public void saveAll(List<Employee> employees) {
		employeeRepository.saveAll(employees);
	}

	@Override
	public void deleteById(int theId) {
		employeeRepository.deleteById(theId);
	}

	@Override
	public Page<Employee> searchBy(String theName,Pageable pageable) {

		Page<Employee> results = null;
		
		if (theName != null && (theName.trim().length() > 0)) {
			results = employeeRepository.findByFirstNameContainsOrLastNameContainsAllIgnoreCase(theName, theName,pageable);
		}
		else {
			results = employeeRepository.findAllByOrderByLastNameAsc(pageable);
		}
		
		return results;
	}


}






