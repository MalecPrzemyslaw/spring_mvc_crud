package com.MPrzem.EmployeesMVCCrud.dataSourceRouting;

import com.MPrzem.EmployeesMVCCrud.entity.Department;
import com.MPrzem.EmployeesMVCCrud.entity.Employee;
import com.MPrzem.EmployeesMVCCrud.service.DepartmentService;
import com.MPrzem.EmployeesMVCCrud.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.MPrzem.EmployeesMVCCrud.dataSourceRouting.DbRouter.DEMOUSERNAME;

@Component
@Slf4j
public class DemoUserHander implements AuthenticationSuccessHandler {
    EmployeeService employeeService;
    DepartmentService departmentService;
    UrlPathHelper urlPathHelper;
    public DemoUserHander(EmployeeService employeeService, DepartmentService departmentService,UrlPathHelper urlPathHelper) {
        this.employeeService = employeeService;
        this.departmentService = departmentService;
        this.urlPathHelper = urlPathHelper;
    }
    @Autowired
    public DemoUserHander(EmployeeService employeeService, DepartmentService departmentService) {
        this.employeeService = employeeService;
        this.departmentService = departmentService;
        this.urlPathHelper = new UrlPathHelper();
    }
    @Transactional
    private void GenerateDemoData(){
        employeeService.deleteAll();
        departmentService.deleteAll();

        ArrayList<Department> departments = new ArrayList<>();
        Department sales = new Department();
        sales.setName("Sales");
        sales.setDescription("This department is responsible for selling the relevant products to the consumers.");
        Department finance = new Department();
        finance.setName("Finance");
        finance.setDescription("The Finance Department is responsible for acquiring and utilizing money for financing the activities of the tourism business.");
        Department general =new Department();
        general.setName("General");
        general.setDescription("This department develops and executes overall business strategies. It is responsible for the entire organization.");
        departments.add(sales);
        departments.add(finance);
        departments.add(general);
        departmentService.saveAll(departments);

        List employees = new ArrayList();
        employees.add(new Employee("Nicholas","Richards","nicholas.richards@example.com",sales,
        2000,LocalDate.of(2014,06,14)));
        employees.add(new Employee("Patrick","Kim","patrick.pearson@example.com",finance,
            300,LocalDate.of(2014,06,24)));
        employees.add(new Employee("Daisy","Pearson","daisy.kim@example.com",finance,
                3000,LocalDate.of(2017,01,01)));
        employees.add(new Employee("Patrick","Pearson","patrick.pearson@example.com",finance,
                4000,LocalDate.of(2014,06,24)));
        employees.add(new Employee("Katie","Pearson","katie.pearson@example.com",finance,
                5000,LocalDate.of(2014,06,24)));
        employees.add(new Employee("Danny","Gutierrez","danny.gutierrez@example.com",general,
                2500,LocalDate.of(2021,04,05)));
        employees.add(new Employee("Amanda","Brooks","amanda.brooks@example.com",sales,
                2700,LocalDate.of(2021,03,05)));
        employeeService.saveAll(employees);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.sendRedirect(urlPathHelper.getContextPath(request));
        if(SecurityContextHolder.getContext().getAuthentication().getName().equals(DEMOUSERNAME)){
            log.info("Demo User logged!!");
            GenerateDemoData();
        }
    }
}
