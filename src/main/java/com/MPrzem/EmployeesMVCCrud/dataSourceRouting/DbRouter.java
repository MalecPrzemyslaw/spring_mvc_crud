package com.MPrzem.EmployeesMVCCrud.dataSourceRouting;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.security.core.context.SecurityContextHolder;

public class DbRouter extends AbstractRoutingDataSource {
    public static final String DEMOUSERNAME="demo_user";
    @Override
    protected Object determineCurrentLookupKey() {
        try{
            if(SecurityContextHolder.getContext().getAuthentication().getName().equals(DEMOUSERNAME)) {
                return DbUsersType.DEMO_CLIENT;
            }else {
                return DbUsersType.NORMAL_CLIENT;
            }
        }catch (Exception e){
            return DbUsersType.NORMAL_CLIENT;
        }

    }
}