package com.MPrzem.EmployeesMVCCrud.aop;

import org.springframework.stereotype.Component;

@Component
public class VisitsCounter {
    private int count=0;

    public synchronized int getCount() {
        return count;
    }

    public synchronized void incrementCount() {
        this.count++;
    }
}
