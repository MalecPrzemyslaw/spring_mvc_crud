package com.MPrzem.EmployeesMVCCrud.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class EntryAspect {
    @Autowired
    VisitsCounter counter;

    @Before("@annotation(entryPointAnnotation)")
    public void beforeEntry(EntryPointAnnotation entryPointAnnotation){
        counter.incrementCount();
        log.info("New visit, visits total: {}",counter.getCount());
    }
}
