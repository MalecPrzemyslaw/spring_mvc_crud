package com.MPrzem.EmployeesMVCCrud.externalApi;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

//wrapped RestTemplate just for easy replacement if needed
public class ApiClient {
    public ApiClient(String generalUrl) {
        this.generalUrl = generalUrl;
        headers = new HttpHeaders();
    }
    public ApiClient(String generalUrl,HttpHeaders headers) {
        this.generalUrl = generalUrl;
        this.headers = headers;
    }
    private String generalUrl;
    private RestTemplate restTemplate = new RestTemplate();

    private HttpHeaders headers;

    public <T> T callGetMethod(String url, Class<T> responseType, Object... objects) {
        HttpEntity<T> requestEntity = new HttpEntity<>(headers);
        var response = restTemplate.exchange(
                generalUrl+url, HttpMethod.GET, requestEntity, responseType, objects);
        return response.getBody();
    }
}
