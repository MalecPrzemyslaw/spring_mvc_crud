package com.MPrzem.EmployeesMVCCrud.externalApi.dto;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WeatherDto {
    float temp;
    int pressure;
    int humidity;
    private float speed;
    private String description;
    public WeatherDto(WeatherDto source){
        temp = source.getTemp();
        pressure = source.getPressure();
        humidity = source.getHumidity();
        speed = source.getSpeed();
        description = source.getDescription();
    }
}
