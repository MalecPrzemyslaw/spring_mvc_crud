package com.MPrzem.EmployeesMVCCrud.externalApi.dto;

import lombok.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurrencyDto {
    double chf;
    double eur;
    double gbp;
    double pln;
    public CurrencyDto(CurrencyDto source){
        chf=source.getChf();
        eur=source.getEur();
        gbp=source.getGbp();
        pln=source.getPln();
    }
}
