package com.MPrzem.EmployeesMVCCrud.externalApi;

import com.MPrzem.EmployeesMVCCrud.externalApi.dto.CurrencyDto;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Collectors;

public class CurrencyClient {
//    String currencies2Querry = Arrays.stream(Rates.class.getDeclaredFields()).map(Field::getName).collect(Collectors.joining(","));
    private final String API_KEY;
    public CurrencyClient(@Value("${currency.generalUrl}") String generalUrl, @Value("${currency.apiKey}") String apiKey){
        API_KEY = apiKey;
        apiClient = new ApiClient(generalUrl);
    }
    public CurrencyClient(ApiClient apiClient){
        this.apiClient = apiClient;
        API_KEY="";
    }
    private ApiClient apiClient;
    public CurrencyDto getCurrentCurrency(){
        CurrencyPojo currencyPojo =  apiClient.callGetMethod("{api_key}/latest/USD",
                CurrencyPojo.class,
                API_KEY);
        return CurrencyDto.builder()
                .eur(currencyPojo.getConversion_rates().getEUR())
                .gbp(currencyPojo.getConversion_rates().getGBP())
                .pln(currencyPojo.getConversion_rates().getPLN())
                .chf(currencyPojo.getConversion_rates().getCHF())
                .build();
    }
    @Getter
    @Setter
    public static class CurrencyPojo{
        public Rates conversion_rates;
    }
    @Getter
    @Setter
    public static class Rates{
        public double CHF;
        public double EUR;
        public double GBP;
        public double PLN;
    }
}
