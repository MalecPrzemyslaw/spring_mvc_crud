package com.MPrzem.EmployeesMVCCrud.externalApi;

import lombok.*;
import com.MPrzem.EmployeesMVCCrud.externalApi.dto.WeatherDto;
import org.springframework.beans.factory.annotation.Value;

public class WeatherClient {
    private final String API_KEY;
    public WeatherClient(@Value("${weather.generalUrl}") String generalUrl, @Value("${weather.apiKey}") String apiKey){
        API_KEY = apiKey;
        apiClient = new ApiClient(generalUrl);
    }
    private ApiClient apiClient;

    public WeatherClient(ApiClient apiClient,String apiKey){
        API_KEY = apiKey;
        this.apiClient = apiClient;
    }
    public WeatherDto getCurrentWeather(String city){
        WeatherPOJO weatherPOJO =  apiClient.callGetMethod("weather?q={city}&appid={apiKey}&units=metric&lang=pl",
                WeatherPOJO.class,city,API_KEY);
        return WeatherDto.builder()
                .temp(weatherPOJO.getMain().getTemp())
                .pressure(weatherPOJO.getMain().getPressure())
                .humidity(weatherPOJO.getMain().getHumidity())
                .speed(weatherPOJO.getWind().getSpeed())
                .description(weatherPOJO.getWeather()[0].getDescription()).build();
    }
    @Getter
    @NoArgsConstructor
    public static class WeatherDescription{
        public WeatherDescription(String description) {
            this.description = description;
        }

        private String description;
    }
    @Getter
    @Setter
    public static class Main{
        private float temp;
        private int pressure;
        private int humidity;
    }
    @Getter
    @Setter
    public static class Wind{
        private float speed;
    }
    @Getter
    @Setter
    public static class WeatherPOJO{
        private Wind wind;
        private Main main;
        private WeatherDescription[] weather;
    }
}
