package com.MPrzem.EmployeesMVCCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class EmployeesMVCCrud {

	public static void main(String[] args) {
		SpringApplication.run(EmployeesMVCCrud.class, args);
	}

}

