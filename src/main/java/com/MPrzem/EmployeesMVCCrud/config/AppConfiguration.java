package com.MPrzem.EmployeesMVCCrud.config;

import com.MPrzem.EmployeesMVCCrud.dataSourceRouting.DbRouter;
import com.MPrzem.EmployeesMVCCrud.dataSourceRouting.DbUsersType;
import com.MPrzem.EmployeesMVCCrud.externalApi.ApiClient;
import com.MPrzem.EmployeesMVCCrud.externalApi.CurrencyClient;
import com.MPrzem.EmployeesMVCCrud.externalApi.WeatherClient;
import com.MPrzem.EmployeesMVCCrud.statistics.StatisticsCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableScheduling
public class AppConfiguration {
    @Autowired
    private Environment env;
    @Bean
    public CurrencyClient  currencyClient(){
        return new CurrencyClient(env.getProperty("currency.generalUrl"),env.getProperty("currency.apiKey"));
    }
    @Bean
    public WeatherClient weatherClient(){
        var apiClient = new ApiClient(env.getProperty("weather.generalUrl"));
        return new WeatherClient(apiClient,env.getProperty("weather.apiKey"));
    }
    @Bean
    public StatisticsCalculator statisticsCalculator() {
        return new StatisticsCalculator();
    }

    @Bean
    public DataSource clientDatasource() {
        Map<Object, Object> targetDataSources = new HashMap<>();
        DataSource demoDatasource = demoDatasource();
        DataSource normalDatasource = normalDatasource();
        targetDataSources.put(DbUsersType.DEMO_CLIENT,
                demoDatasource);
        targetDataSources.put(DbUsersType.NORMAL_CLIENT,
                normalDatasource);

        DbRouter clientRoutingDatasource
                = new DbRouter();
        clientRoutingDatasource.setTargetDataSources(targetDataSources);
        clientRoutingDatasource.setDefaultTargetDataSource(normalDatasource);

        return clientRoutingDatasource;
    }
    private DataSource demoDatasource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("dbDriver"));
        dataSourceBuilder.url(env.getProperty("dbDemoUrl"));
        dataSourceBuilder.username(env.getProperty("dbUserName"));
        dataSourceBuilder.password(env.getProperty("dbUserPassword"));
        return dataSourceBuilder.build();
    }

    private DataSource normalDatasource() {
DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty("dbDriver"));
        dataSourceBuilder.url(env.getProperty("dbNormalUrl"));
        dataSourceBuilder.username(env.getProperty("dbUserName"));
        dataSourceBuilder.password(env.getProperty("dbUserPassword"));
        return dataSourceBuilder.build();
    }
}