package com.MPrzem.EmployeesMVCCrud.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;

@Configuration
@EnableWebSecurity
@Profile("test")
public class SecurityTestConfig extends SecurityConfig{
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        User.UserBuilder users = User.withDefaultPasswordEncoder();
        auth.inMemoryAuthentication()
                .withUser(users.username("guest").password("guest").roles("GUEST"))
                .withUser(users.username("employee").password("employee").roles("EMPLOYEE"))
                .withUser(users.username("manager").password("manager").roles("EMPLOYEE", "MANAGER"))
                .withUser(users.username("admin").password("admin").roles("EMPLOYEE", "ADMIN"));
    }

}
