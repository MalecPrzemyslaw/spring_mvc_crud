-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: employee_directory
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE USER 'mvc_client'@'%' IDENTIFIED BY "123password890";
CREATE DATABASE mvc_app_db;
CREATE DATABASE mvc_app_db_demo;
GRANT ALL PRIVILEGES ON mvc_app_db.* To 'mvc_client'@'%';
GRANT ALL PRIVILEGES ON mvc_app_db_demo.* To 'mvc_client'@'%';
USE mvc_app_db;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Sales','This department is responsible for selling the relevant products to the consumers.'),
    (2,'Finance','The Finance Department is responsible for acquiring and utilizing money for financing the activities of the tourism business.'),
    (3,'General','This department develops and executes overall business strategies. It is responsible for the entire organization.');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `salary` int NOT NULL,
  `employment_date` date NOT NULL,
  `department_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Nicholas','Richards','nicholas.richards@example.com',2000,'2011-06-14',1),
    (2,'Patrick','Pearson','patrick.pearson@example.com',3000,'2014-06-24',2),
    (3,'Daisy','Kim','daisy.kim@example.com',5000,'2017-01-01',3),
    (4,'Katie','Pearson','katie.pearson@example.com',5600,'2020-03-05',3),
    (5,'Danny','Gutierrez','danny.gutierrez@example.com',2500,'2021-04-05',1),
    (6,'Amanda','Brooks','amanda.brooks@example.com',2700,'2021-03-05',1);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;
--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','$2a$10$eub631XKSn9WdurnBrcMTe7BEugy58GuJ2/rO3CoTpvuKIbDeHb8O',1,'ADMIN,EMPLOYEE'),
    (2,'employee','$2a$10$pG7mwmGNoeUiT8cYnNo1f.8BJBHGSuzpOmRnm5/tTM.LAveWFw/DG',1,'EMPLOYEE'),
    (3,'manager','$2a$10$pLd/C6CDR6JmZeJymjxxOuJ3WhoIdw8GRDwjOBRdL3U5OiUMlWKUW',1,'MANAGER,EMPLOYEE'),
    (4,'guest','$2a$10$OKV4c2H9DlabAdIvLyECw.4BuszjxaPwbkxYFf5dzhO.8tq3rrCCK',1,'GUEST'),
    (5,'demo_user','$2a$10$N0ErNGmTp9NqC6KUdxkm9.Md44X7s9atYVKTTRHILi7bW95SCOhKS',1,'GUEST,ADMIN,MANAGER,EMPLOYEE');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

use mvc_app_db_demo;

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES
    (1,'Sales','This department is responsible for selling the relevant products to the consumers.'),
    (2,'Finance','The Finance Department is responsible for acquiring and utilizing money for financing the activities of the tourism business.'),
    (3,'General','This department develops and executes overall business strategies. It is responsible for the entire organization.');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `salary` int NOT NULL,
  `employment_date` date NOT NULL,
  `department_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Nicholas','Richards','nicholas.richards@example.com',2000,'2011-06-14',1),
    (2,'Patrick','Pearson','patrick.pearson@example.com',3000,'2014-06-24',2),
    (3,'Daisy','Kim','daisy.kim@example.com',5000,'2017-01-01',3),
    (4,'Katie','Pearson','katie.pearson@example.com',5600,'2020-03-05',3),
    (5,'Danny','Gutierrez','danny.gutierrez@example.com',2500,'2021-04-05',1),
    (6,'Amanda','Brooks','amanda.brooks@example.com',2700,'2021-03-05',1);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;
--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','$2a$10$eub631XKSn9WdurnBrcMTe7BEugy58GuJ2/rO3CoTpvuKIbDeHb8O',1,'ADMIN,EMPLOYEE'),
    (2,'employee','$2a$10$pG7mwmGNoeUiT8cYnNo1f.8BJBHGSuzpOmRnm5/tTM.LAveWFw/DG',1,'EMPLOYEE'),
    (3,'manager','$2a$10$pLd/C6CDR6JmZeJymjxxOuJ3WhoIdw8GRDwjOBRdL3U5OiUMlWKUW',1,'MANAGER,EMPLOYEE'),
    (4,'guest','$2a$10$OKV4c2H9DlabAdIvLyECw.4BuszjxaPwbkxYFf5dzhO.8tq3rrCCK',1,'GUEST'),
    (5,'demo_user','$2a$10$N0ErNGmTp9NqC6KUdxkm9.Md44X7s9atYVKTTRHILi7bW95SCOhKS',1,'GUEST,ADMIN,MANAGER,EMPLOYEE');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-15 14:51:13